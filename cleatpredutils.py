'''
Created on 5 May 2014

@author: Chris
'''

import urllib2
import json
from datetime import date, timedelta
from sklearn import linear_model as lm
import numpy as np
import pandas as pd
from pandas import DataFrame as df

trainingDataFile = 'D:\\Users\\Chris\\workspace\\cleatpredictor\\resources\\data.csv'


def get_new_features(thisDate = date.today()):
    url= 'https://www.cl.cam.ac.uk/research/dtg/weather/daily-text.cgi?'
    
    if thisDate == date.today():
        response = urllib2.urlopen('http://api.openweathermap.org/data/2.5/forecast?q=cambridge,uk&mode=json')
        today = json.loads(response.read())
        
        # find today's projected temperature and rainfall
        count, rain, temp = 0, 0, []
        for item in today['list']:
            count += 1
            if count <= 5: #if time is before 1500
                rain += float(item['rain']['3h'])
            if 3 <= count <= 6: #if time is between 0900 and 1800
                temp += [float(item['main']['temp_min'])-273.15, float(item['main']['temp_max'])-273.15]
            if count > 6:
                break
        
        todayRain = rain
        todayTemp = sum(temp)/len(temp)
    else:
        thisUrl = url + thisDate.isoformat()
        data = [line.split('\t') for line in urllib2.urlopen(thisUrl).readlines() if line[0] != '#']
        
        count, temp = 0, []
        for row in data:
            count += 1
            if 18 <= count <= 36: #if time is between 0900 and 1800
                temp += [float(row[1])]
    
        todayRain, todayTemp = float(data[30][8]), sum(temp)/len(temp)
    
    featureList = [todayRain, todayTemp]
    
    #find weather for previous 14 days
    for i in range(14):
        thisDate -= timedelta(1)
        thisUrl = url + thisDate.isoformat()
        data = [line.split('\t') for line in urllib2.urlopen(thisUrl).readlines() if line[0] != '#']
        
        count, temp = 0, []
        for row in data:
            count += 1
            if 18 <= count < 42: #if time is between 0900 and 2100
                temp += [float(row[1])]
    
        featureList += [float(data[-1][8]), sum(temp)/len(temp)]
    
    featureList.reverse()
    return featureList

def add_data(grade, thisDate = date.today(), reporter='anon'):
    data = pd.read_csv(trainingDataFile)
    colNames = list(data.columns.values)
    rowData = get_new_features(thisDate) + [thisDate.isoformat(), 0, grade, reporter]
    
    data = data.append(dict(zip(colNames,rowData)),ignore_index=True)
    sameDate = np.where(data['date'] == thisDate.isoformat())[0]
    for ind in sameDate:
        data['weight'][ind] = 1/len(sameDate)
    
    data.to_csv(trainingDataFile, float_format='%2.3f', index = False, header = True)
    
def get_prediction(thisDate=date.today()):
    data = pd.read_csv(trainingDataFile)
    X = np.array(data)[:,0:30]
    weights= np.array(data['weight'])
    y = np.array(data['response'])
    
    clf = lm.Ridge(alpha = 0.5)
    clf.fit(X, y, normalize='True', sample_weight=weights)
    
    prediction = clf.predict(get_new_features(thisDate))
    
    return prediction[0]