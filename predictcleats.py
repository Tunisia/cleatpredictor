'''
Created on 10 May 2014

@author: Chris
'''

import cleatpredutils as utils
import sys
from datetime import date, datetime

def report(grade, thisDate=date.today(), reporter='anon'):
    utils.add_data(grade,thisDate,reporter)
    
def predict(thisDate = date.today()):
    prediction = utils.get_prediction()
    print prediction
    return prediction
    
def parse_input(args):
    for ind in range(1,len(args)):
        if args[ind] == '-date':
            params['date'] = datetime.strptime(args[ind+1],'%Y-%m-%d').date()
        if args[ind] == '-grade':
            params['grade'] = float(args[ind+1])
        if args[ind] == '-reporter':
            params['reporter'] = args[ind+1]
    
args = sys.argv[1:len(sys.argv)]

#sysargv = ['predictcleats.py', '-report', '-grade', '2', '-date', '2014-05-08', '-reporter', 'chris']
#args = sysargv[1:len(sysargv)]

params = {
          'date':date.today(),
          'grade':-1,
          'reporter':'anon'
          }    

parse_input(args)

if args[0] == '-report':
    if 0 <= params['grade'] <= 3.0:
        report(params['grade'],params['date'], params['reporter'])
        print 'Thank you!'
elif args[0] == '-predict':
    prediction = predict(params['date'])
    print 'Prediction of ground softness is', prediction
    print 'Key:\
0 - astroturf shoes\n\
1 - firm ground cleats (blades, wide receiver cleats, molded studs)\n\
2 - soft ground cleats (football boots with metal studs)\n\
3 - rugby boots (full metal studs)'
else:
    failstr = '\
Illegal argument(s).\n\
To submit a report: "-report -grade {grade} [-date {date = today}] [-reporter {you = anon}]"\n\
To get a prediction: "-predict [-date {YYYY-MM-DD}]"\n'
    
    print failstr
    


